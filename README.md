**Explicacion del Codigo en css**


  **header{**

    text-align: center; permite establecer la alineación horizontal de un bloque de texto
    padding: 5px; establece el área de relleno en los cuatro lados de un elemento a la vez.
    background-color: rgb(228, 182, 167); color del area de relleno
    color: white; color de la letra
  }

 **footer{**

    text-align: center; permite establecer la alineación horizontal de un bloque de texto
    padding: 10px; establece el área de relleno en los cuatro lados de un elemento a la vez. 
    background-color: DarkSalmon; color del area de relleno
    color: white; color de la letra
  
}

  **section{**

    margin: 20px; establece el margen para los cuatro lados
  }

  **.contenedor-datos-personales {**

    max-width: 800px; se utiliza para establecer el ancho máximo de un elemento especificado.
    margin: 0 auto; centrara el elemento
    padding: 20px; establece el área de relleno en los cuatro lados de un elemento a la vez. 
    background-color: #f9f9f9; Color del elemento
    border-radius: 5px; nos permite redondear los bordes de las cajas
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1); Sombras
  }
**.imagen-principal{**

    width: 200px; establece el ancho de un elemento
    border-radius: 50%; nos permite redondear los bordes de las cajas
    margin: 10px auto; establece el margen para los cuatro lados
    display: flex; nos ayuda a controlar dónde se va a ver un elemento
  }
  
  **.titulo-seccion {**

    text-align: center; permite establecer la alineación horizontal de un bloque de texto
    font-size: 24px; especifica la dimensión de la letra
    margin: 20px 0; establece el margen para los cuatro lados
  }
  
**.dato{**

    display: flex; nos ayuda a controlar dónde se va a ver un elemento
    align-items: center; establece un valor en todos los grupos 
    margin: 10px 0; establece el margen para los cuatro lados
  }
  
  **.dato span{**

    flex-basis: 200px; especifíca la base flexible
    font-weight: bold; especifica el peso o grueso de la letra
  }
  
  **.dato a {**

    text-decoration: none; se usa para establecer el formato de texto a subrayado
    color: #333; color de la letra
    margin-left: 10px; establece el margen izquierdo de un elemento especificando una longitud o un porcentaje
  }
  
  
  **.contenedor-secciones{**

    display: flex; nos ayuda a controlar dónde se va a ver un elemento
    justify-content: space-between; define cómo el navegador distribuye el espacio entre y alrededor de los items flex
    margin-top: 20px; establece el margen superior de un elemento especificando una longitud o un porcentaje
  }
  
  **#experiencia-laboral,
  #capacitaciones {**

    flex-basis: 48%; especifíca la base flexible, la cual es el tamaño inicial de un elemento flexible
  }
  
  
  **ul{**

    margin: 0; establece el margen para los cuatro lados
    padding: 0; establece el área de relleno en los cuatro lados de un elemento a la vez.
    list-style-type: none; estilo de tipo de lista
  }
  
  **li{**

    margin-bottom: 5px; establece el espacio requerido en la parte inferior de un elemento
  }
  
  **li::before{**

    content: "→ "; icono de la lista
  }
  
  **.tabla{**

    margin: 0 auto; centrara el elemento
    display: flex; nos ayuda a controlar dónde se va a ver un elemento
    flex-direction: column; especifica cómo colocar los objetos flexibles en el contenedor flexible definiendo el eje principal y la dirección
    border-collapse: collapse; se utiliza para fusionar los bordes
    width: 70%; establece el ancho de un elemento
    padding: 40px; establece el área de relleno en los cuatro lados de un elemento a la vez.
 
  }
  
**.fila{**

    display: flex; establece el área de relleno en los cuatro lados de un elemento a la vez.
    flex-direction: row;
    border-bottom: 1px solid #ccc;
    padding: 10px; establece el área de relleno en los cuatro lados de un elemento a la vez.
  }
  
  **.encabezado{**

    font-weight: bold; especifica el peso o grueso de la letra
    background-color: #eee; Color del elemento
  }
  
  **.columna{**

    flex: 1; indica la capacidad de un elemento flexible para alterar sus dimensiones
    text-align: center; permite establecer la alineación horizontal de un bloque de texto
  }
  
